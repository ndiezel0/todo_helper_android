package com.smth.anonim.todohelper;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.scalified.fab.ActionButton;
import com.smth.anonim.todohelper.models.Project;
import com.smth.anonim.todohelper.models.Todo;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MainActivity extends AppCompatActivity {

    private ListAdapter mAdapter;

    final private String SERVER_URI = "https://floating-ocean-93940.herokuapp.com";
    private ArrayList<Project> _projects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        setContentView(R.layout.activity_main);
        setTitle("Задачи");

        mAdapter = new ListAdapter(this, SERVER_URI);
        /*final ArrayList<String> headers = new ArrayList<String>();
        headers.add("Семья");
        headers.add("Работа");
        headers.add("Прочее");
        for (int i = 0; i < 30; i++) {
            if (i % 4 == 0) {
            mAdapter.addSectionHeaderItem("Section #" + i);
        }
            mAdapter.addItem("Row Item #" + i, Boolean.valueOf(i%3 == 0));

        }*/
        ListView listView = (ListView) findViewById(R.id.todo_list);
        listView.setAdapter(mAdapter);
        updateListView();
        ActionButton fab = (ActionButton) findViewById(R.id.action_button);

        fab.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                ArrayList<String> a = new ArrayList<String>();
                ArrayList<Integer> b = new ArrayList<Integer>();
                if(_projects == null) return;
                for(Project project: _projects){
                    a.add(project.getTitle());
                    b.add(project.getId());
                }
                bundle.putStringArrayList("title", a);
                bundle.putIntegerArrayList("id", b);
                bundle.putString("path", SERVER_URI);
                Intent intent = new Intent(v.getContext(), NewTodoActivity.class);
                intent.putExtras(bundle);
                startActivityForResult(intent, 2);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 2)
        {
            Handler h = new Handler();
            h.postDelayed(new Runnable() {
                @Override
                public void run() {
                    updateListView();
                }
            }, 400);
        }
    }

    private void updateListView(){
        mAdapter.clear();
        Ion.with(this)
                .load(SERVER_URI + "/todos.json")
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        int i = 1;
                        if (result != null) {
                            ArrayList<Project> projects = new ArrayList<Project>();
                            _projects = projects;
                            for (final JsonElement projectJsonElement : result) {
                                projects.add(new Gson().fromJson(projectJsonElement, Project.class));
                            }
                            for(Project project : projects){
                                mAdapter.addSectionHeaderItem(project.getTitle());
                                for(Todo todo : project.getTodos()){
                                    mAdapter.addItem(todo.getText(), todo.getCompleted(), todo.getId());
                                }
                            }
                        }
                        else {
                            updateListView();
                        }
                    }
                });
        mAdapter.notifyDataSetChanged();
    }
}


