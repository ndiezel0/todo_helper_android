package com.smth.anonim.todohelper.models;

import java.util.ArrayList;

/**
 * Created by anonim on 06.06.2017.
 */

public class Project {
    private int id;
    private String title;
    private ArrayList<Todo> todos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Todo> getTodos() {
        return todos;
    }

    public void setTodos(ArrayList<Todo> todos) {
        this.todos = todos;
    }
}
