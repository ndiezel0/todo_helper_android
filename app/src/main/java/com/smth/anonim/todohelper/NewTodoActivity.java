package com.smth.anonim.todohelper;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

public class NewTodoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_todo);
        setTitle("Новая Задача");
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            ArrayList<String> projectsArray = getIntent().getExtras().getStringArrayList("title");
            final ArrayList<Integer> idArray = getIntent().getExtras().getIntegerArrayList("id");

            final ListView projectsList = (ListView) findViewById(R.id.projectsList);
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.projectlist_cell);
            adapter.addAll(projectsArray);
            projectsList.setAdapter(adapter);

            projectsList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            final EditText editText = (EditText) findViewById(R.id.editText);
            ImageButton imageButton = (ImageButton) findViewById(R.id.imageButton);

            final ArrayList<PosId> posId = new ArrayList<>();
            int i = 0;
            for(int id: idArray){
                PosId pos = new PosId();
                pos.setId(id);
                pos.setPosition(i); i++;
                pos.setSelected(false);
                posId.add(pos);
            }
            projectsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    CheckedTextView tw = (CheckedTextView) view;
                    PosId twPos = null; int old_position = -1;
                    for(PosId pos: posId){
                        if (pos.getPosition() == position){
                            twPos = pos;
                        }
                        if (pos.isSelected()){
                            old_position = pos.getPosition();
                            pos.setSelected(false);
                        }
                    }
                    CheckedTextView ptw = (CheckedTextView)parent.getChildAt(old_position);
                    if(ptw != null) ptw.setCheckMarkTintList(null);
                    tw.setCheckMarkTintList(getResources().getColorStateList(R.color.systemBlackColor));
                    if (twPos != null) twPos.setSelected(true);
                }
            });
            imageButton.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    JsonObject json = new JsonObject();
                    json.addProperty("text", editText.getText().toString());
                    PosId result = null;
                    for(PosId pos : posId){
                        if(pos.isSelected()) result = pos;
                    }
                    if(result == null) return;
                    json.addProperty("project_id", result.getId());
                    Ion.with(v.getContext())
                            .load(getIntent().getExtras().getString("path") + "/todos")
                            .setJsonObjectBody(json)
                            .asString()
                            .setCallback(new FutureCallback<String>() {
                                @Override
                                public void onCompleted(Exception e, String result) {
                                }
                            });
                    setResult(2);
                    finish();
                }
            });
        }
        catch (Exception e){
            finish();
        }

    }
    private class PosId{
        private int id;
        private int position;
        private boolean selected;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getPosition() {
            return position;
        }

        public void setPosition(int position) {
            this.position = position;
        }

        public boolean isSelected() {
            return selected;
        }

        public void setSelected(boolean selected) {
            this.selected = selected;
        }
    }

}
