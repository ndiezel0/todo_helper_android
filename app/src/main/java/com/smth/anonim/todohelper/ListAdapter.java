package com.smth.anonim.todohelper;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeSet;

import static com.smth.anonim.todohelper.R.id.textView;

/**
 * Created by anonim on 04.06.2017.
 */

public class ListAdapter extends BaseAdapter {
    private static final int TYPE_ITEM = 0;
    private static final int TYPE_SEPARATOR = 1;
    private String SERVER_URI;

    private ArrayList<String> mData = new ArrayList<String>();
    private TreeSet<Integer> sectionHeader = new TreeSet<Integer>();
    private HashMap<String, Boolean> checkMap = new HashMap<String, Boolean>();
    private HashMap<String, Integer> idMap = new HashMap<String, Integer>();

    private LayoutInflater mInflater;

    public ListAdapter(Context context, String path) {
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        SERVER_URI = path;
    }

    public void addItem(final String item, Boolean status, Integer id) {
        mData.add(item);
        checkMap.put(item, status);
        idMap.put(item, id);
        notifyDataSetChanged();
    }

    public void addSectionHeaderItem(final String item) {
        mData.add(item);
        sectionHeader.add(mData.size() - 1);
        notifyDataSetChanged();
    }

    public void clear(){
        mData.clear();
        sectionHeader.clear();
        checkMap.clear();
        idMap.clear();
    }

    @Override
    public int getItemViewType(int position) {
        return sectionHeader.contains(position) ? TYPE_SEPARATOR : TYPE_ITEM;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public String getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        int rowType = getItemViewType(position);

        if (convertView == null) {
            holder = new ViewHolder();
            switch (rowType) {
                case TYPE_ITEM:
                    convertView = mInflater.inflate(R.layout.common_item, null);
                    holder.textView = (TextView) convertView.findViewById(R.id.text);
                    holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBox);
                    final TextView specialTW = (TextView) convertView.findViewById(R.id.text);
                    holder.checkBox.setOnClickListener( new View.OnClickListener() {
                        public void onClick(View v) {
                            CheckBox cb = (CheckBox) v ;
                            TextView textView = specialTW;
                            if(cb.isChecked()){
                                textView.setPaintFlags(textView.getPaintFlags() |
                                        Paint.STRIKE_THRU_TEXT_FLAG);
                            }
                            else{
                                textView.setPaintFlags(textView.getPaintFlags() &
                                        ~(Paint.STRIKE_THRU_TEXT_FLAG));
                            }
                            Ion.with(v.getContext())
                                    .load(SERVER_URI + "/todos/" + idMap.get(mData.get(position)) + "/edit")
                                    .asString()
                                    .setCallback(new FutureCallback<String>() {
                                        @Override
                                        public void onCompleted(Exception e, String result) {
                                        }
                                    });

                            //TODO sending something to rails
                        }
                    });
                    break;
                case TYPE_SEPARATOR:
                    convertView = mInflater.inflate(R.layout.head_item, null);
                    holder.textView = (TextView) convertView.findViewById(R.id.textSeparator);
                    break;
            }
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textView.setText(mData.get(position));
        if(rowType == TYPE_ITEM){
            holder.checkBox.setChecked(checkMap.get(mData.get(position)));
            if(checkMap.get(mData.get(position))){
                holder.textView.setPaintFlags(holder.textView.getPaintFlags() |
                        Paint.STRIKE_THRU_TEXT_FLAG);
            }
            else{
                holder.textView.setPaintFlags(holder.textView.getPaintFlags() &
                        ~(Paint.STRIKE_THRU_TEXT_FLAG));
            }
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView textView;
        public CheckBox checkBox;
    }
}
